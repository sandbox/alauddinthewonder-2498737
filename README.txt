CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

Sometimes you need to create date input field but not restrict user to enter in a particular format. Yes it's now possible with the help of this module. This module let user's to input their date in any kind of form and when user blur the field it automatically convert date into admin's desired format. Let's take an example, suppose user input date like 31/12/2015 or 12/31/2015 or 2015/31/12 or 2015/12/31 or 31-12-2015 or 12-31-2015 or 2015-12-31 etc., this program will convert all of them into 12/31/2015 (as admin set it to mm/dd/yyyy format).

The Date Formatter module is a wrapper for the DateFormatter jQuery Plugin by Alauddin Ansari,
https://github.com/AlauddinTheWonder/jQuery-DateFormatter. It allows a user to more easily
enter any form of date and it'll converted into correct format.


INSTALLATION
------------

The Date Formatter Module required no dependencies. That's mean it can by installed individually.

Enable the modules from the Administration >> Modules page.

After that goto module's config page 
Administration >> Configuration >> User Interface >> Date Formatter Settings
(admin/config/user-interface/dataformatter)

and set desired date format (default is mm/dd/yyyy) and 

finally the important setting is the input field's IDs to be typed in the textarea.


USAGE
-----

Basically you have to take every field's ID on which you need to assign date formatter, and put it in the textarea (admin/config/user-interface/dataformatter) with # prefix and separate with commas.
Ex. #inputfield1, #inputfield2... so on